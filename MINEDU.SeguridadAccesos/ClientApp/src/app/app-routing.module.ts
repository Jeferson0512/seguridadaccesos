import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CounterComponent } from './counter/counter.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';

const routes: Routes = [
  //{ path: '', component: HomeComponent, pathMatch: 'full' },
  { path: '', loadChildren: () => import('./module/home/home.module').then(m => m.HomeModule) },
  { path: 'auth', loadChildren: () => import('./module/auth/auth.module').then(m => m.AuthModule) },
  { path: 'solicitud', loadChildren: () => import('./module/request/request.module').then(m => m.RequestModule) },
  { path: 'dashboard', loadChildren: () => import('./module/dashboard/dashboard.module').then(m => m.DashboardModule) },
  { path: 'counter', component: CounterComponent},
  { path: 'fetch-data', component: FetchDataComponent },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: ''
  }
  //{ path: 'login', component: LoginComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
