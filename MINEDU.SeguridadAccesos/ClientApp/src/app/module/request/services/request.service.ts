import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ItemsModel } from 'src/app/core/models/items.model';
import { isTemplateSpan } from 'typescript';
import { environment } from '../../../../environments/environment';

import * as dataPerson from '../../../data/Persona.json';

@Injectable({
  providedIn: 'root'
})
export class RequestService {
  private readonly URL = environment.api;

  fecha: Date = new Date()

  callback: EventEmitter<any> = new EventEmitter<any>()

  constructor(private _http: HttpClient) { }

  getAllData(id:number){

  }

  getAllPerson(): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    return this._http.post(`${this.URL}/api/Autorizacion/autorizacion/ObtenerTodos`, {
      maxResultCount: 1000,
      skipCount: 0,
      sorting: "",
      filter: ""
    }, { headers: headers })
  }
  getAllDetailPerson(id: number): Observable<any> {
    return this._http.post(`${this.URL}/api/Autorizacion/autorizacion/GetForEdit`, {
      id: id
    })
  }

  sendItemsRequest(Items: ItemsModel[]): Observable<any>{
    console.log(JSON.stringify({Items}))
    return this._http.post(`${this.URL}/api/Autorizacion/autorizacion/CreateOrUpdate`, JSON.stringify({
      Items
    }));
    // id: Items.id,
    //   person: {
    //     id: Items.person.id
    //   },
    //   week: {
    //     id: Items.week.id
    //   },
    //   state: Items.state,
    //   firstLevelOrgan: {
    //     id: Items.firstLevelOrgan.id
    //   },
    //   secondLevelOrgan: {
    //     id: Items.secondLevelOrgan.id
    //   },
    //   thirdLevelOrgan: {
    //     id: Items.thirdLevelOrgan.id
    //   },
    //   authorizationDetail:{
    //     order: Items.authorizationDetail.order,
    //     floor: Items.authorizationDetail.floor,
    //     presentialDays: Items.authorizationDetail.presentialDays,
    //     monday: Items.authorizationDetail.monday,
    //     tuesday: Items.authorizationDetail.tuesday,
    //     wednesday: Items.authorizationDetail.wednesday,
    //     thursday: Items.authorizationDetail.thursday,
    //     friday: Items.authorizationDetail.friday,
    //     saturday: Items.authorizationDetail.saturday,
    //     sunday: Items.authorizationDetail.sunday,
    //     headquarterId: Items.authorizationDetail.headquarterId
    //   }
  }
}
