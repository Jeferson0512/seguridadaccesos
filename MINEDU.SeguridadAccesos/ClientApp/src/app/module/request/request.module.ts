import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { RequestRoutingModule } from './request-routing.module';
import { RequestComponent } from './page/request/request.component';
import { SharedModule } from '../../shared/shared.module';
import { RequestInternalComponent } from './page/request-internal/request-internal.component';
import { RequestExternalComponent } from './page/request-external/request-external.component';


@NgModule({
  declarations: [RequestComponent, RequestInternalComponent, RequestExternalComponent],
  imports: [
    CommonModule,
    RequestRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class RequestModule { }
