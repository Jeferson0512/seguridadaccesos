import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RequestExternalComponent } from './page/request-external/request-external.component';
import { RequestInternalComponent } from './page/request-internal/request-internal.component';
import { RequestComponent } from './page/request/request.component';


const routes: Routes = [
  { path: '', component: RequestComponent },
  { path: 'personal-interno', component: RequestInternalComponent},
  { path: 'personal-externo', component: RequestExternalComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequestRoutingModule { }
