import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.css']
})
export class RequestComponent implements OnInit {

  radPersonaInterno: number = 0
  radPersonaExterno: number = 0
  urlSolicitud: string = ''

  constructor(private router: Router) { }

  ngOnInit() {
  }

  public VerificarRequest() {
    if (this.radPersonaInterno == 1) {
      this.router.navigate(['/solicitud/personal-interno'])
    }
    if (this.radPersonaExterno == 2) {
      this.router.navigate(['/solicitud/personal-externo'])
    }
  }
}
