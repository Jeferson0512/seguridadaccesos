import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestExternalComponent } from './request-external.component';

describe('RequestExternalComponent', () => {
  let component: RequestExternalComponent;
  let fixture: ComponentFixture<RequestExternalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestExternalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestExternalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
