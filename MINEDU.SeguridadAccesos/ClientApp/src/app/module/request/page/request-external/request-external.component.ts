import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { RequestService } from "../../services/request.service";
import { FirstLevelOrgansModel } from "../../../../core/models/firstLevelOrgans.model";
import { SecondLevelOrgansModel } from "../../../../core/models/secondLevelOrgans.model";
import { ThirdLevelOrgansModel } from "../../../../core/models/thirdLevelOrgans.model";
import { FormControl, FormGroup } from "@angular/forms";
import { DateModel } from "src/app/core/models/date.model";

@Component({
  selector: "app-request-external",
  templateUrl: "./request-external.component.html",
  styleUrls: ["./request-external.component.css"],
})

export class RequestExternalComponent implements OnInit, OnDestroy {
  idPersona: number = 0;
  idFecha: boolean;
  listObservers$: Array<Subscription> = [];

  //Mockup de la Lista de Personas
  firstLevelModel: FirstLevelOrgansModel[] = [];
  secondLevelModel: Array<SecondLevelOrgansModel> = [];
  thirdLevelModel: Array<ThirdLevelOrgansModel> = [];
  fechaModel: Array<DateModel> = [];

  second: any = {
    id: 0,
  };

  formRequest: FormGroup = new FormGroup({});

  constructor(private _requestService: RequestService) {}

  ngOnInit(): void {
    //Valores del FormGroup
    this.formRequest = new FormGroup({
      cmbWeekId: new FormControl(""),
      cmbFirstId: new FormControl(""),
      cmbSecondId: new FormControl(""),
      cmbThirdId: new FormControl(""),
      cmbHeadId: new FormControl(""),
    });

    const observer$: Subscription = this._requestService.callback.subscribe(
      (response) => {
        console.log("Recibiendo datos");
      }
    );
    this.listObservers$ = [observer$];

    this._requestService
      .getAllDetailPerson(this.second.id)
      .subscribe((response) => {
        this.firstLevelModel = response.result.firstLevelOrgans;
        let fechaStar = response.result.weeks;
        fechaStar.forEach(element => {
          this.fechaModel.push({
            id: element.id,
            value: this.getSelectFecha(element.startDate, element.endDate),
            selected: false
          })
        });
      });
  }

  selectSecondLevel() {
    this._requestService
      .getAllDetailPerson(this.second.id)
      .subscribe((response) => {
        this.secondLevelModel = response.result.secondLevelOrgans;
        this.secondLevelModel = this.secondLevelModel.filter(
          (data) => data.parentId == this.formRequest.controls.cmbFirstId.value
        );
        // console.log("Prueba", this.mockupSecondLevel)
      });
  }

  selectThirdLevel() {
    // console.log("Id Tercero: ", this.formRequest.controls.cmbSecondId.value)
    this._requestService
      .getAllDetailPerson(this.second.id)
      .subscribe((response) => {
        this.thirdLevelModel = response.result.thirdLevelOrgans;
        this.thirdLevelModel = this.thirdLevelModel.filter(
          (data) => data.parentId == this.formRequest.controls.cmbSecondId.value
        );
        // console.log("Prueba", this.mockupThirdLevel)
      });
  }

  getSelectFecha(dtFechaStart: string, dtFechaEnd: string) {
    let dtStart = new Date(dtFechaStart);
    let dtEnd = new Date(dtFechaEnd);
    let diaS = dtStart.getDate();
    let diaE = dtEnd.getDate();
    // dtStart.setMonth(dtStart.getMonth)
    let mesS = dtEnd.getMonth() + 1;
    return `Semana: ${diaS} al ${diaE} de ${this.getNameMonth(mesS)}`;
  }

  getNameMonth(name: number | string) {
    let nombre = "";
    switch (name) {
      case 1:
        name = "Enero";
        break;
      case 2:
        name = "Febrero";
        break;
      case 3:
        name = "Marzo";
        break;
      case 4:
        name = "Abril";
        break;
      case 5:
        name = "Mayo";
        break;
      case 6:
        name = "Junio";
        break;

      case 7:
        name = "Julio";
        break;

      case 8:
        name = "Agosto";
        break;

      case 9:
        name = "Setiembre";
        break;

      case 10:
        name = "Octubre";
        break;

      case 11:
        name = "Noviembre";
        break;

      case 12:
        name = "Diciembre";
        break;
    }
    return name;
  }

  ngOnDestroy(): void {
    this.listObservers$.forEach((u) => u.unsubscribe());
    console.log("Destruido");
  }
}
