import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestInternalComponent } from './request-internal.component';

describe('RequestInternalComponent', () => {
  let component: RequestInternalComponent;
  let fixture: ComponentFixture<RequestInternalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestInternalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestInternalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
