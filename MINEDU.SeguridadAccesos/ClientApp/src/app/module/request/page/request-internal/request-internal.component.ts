import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { AuthorizationModel } from 'src/app/core/models/Authorization.model';
import { DateModel } from 'src/app/core/models/date.model';
import { ItemsModel } from 'src/app/core/models/items.model';
import { FirstLevelOrgansModel } from '../../../../core/models/firstLevelOrgans.model';
import { HeadQuarters } from '../../../../core/models/headQuarters.model';
import { PersonModel } from '../../../../core/models/Person.model';
import { SecondLevelOrgansModel } from '../../../../core/models/secondLevelOrgans.model';
import { ThirdLevelOrgansModel } from '../../../../core/models/thirdLevelOrgans.model';
import { RequestService } from '../../services/request.service';

@Component({
  selector: 'app-request-internal',
  templateUrl: './request-internal.component.html',
  styleUrls: ['./request-internal.component.css']
})
export class RequestInternalComponent implements OnInit {
  //Mockup de la Lista
  fechaModel: Array<DateModel> = [];
  Item: Array<any> = [];
  mockupPeople: PersonModel[] = [];
  firstLevelModel: FirstLevelOrgansModel[] = [];
  secondLevelModel: Array<SecondLevelOrgansModel> = [];
  thirdLevelModel: Array<ThirdLevelOrgansModel> = [];
  headQuartersModel: HeadQuarters[] = [];
  authorizationDetail: AuthorizationModel[] = []

  second: any = {
    id: 0
  }
  idFecha: boolean;

  formRequest: FormGroup = new FormGroup({});

  constructor(private _requestService: RequestService) { }


  //Se cargara al iniciar el componente
  ngOnInit():void {
    // const { items }: any = (dataPerson as any).default
    // this.mockupPeople = items[0].person

    //Valores del FormGroup
    this.formRequest = new FormGroup({
      cmbWeekId: new FormControl('', [
        Validators.required
      ]),
      cmbFirstId: new FormControl('', [
        Validators.required
      ]),
      cmbSecondId: new FormControl('', [
        Validators.required
      ]),
      cmbThirdId: new FormControl('', [
        Validators.required
      ]),
      cmbHeadId: new FormControl('', [
        Validators.required
      ]),
      cmbSedeCampus: new FormControl('', [
        Validators.required
      ]),
      cmbPisoPabellon: new FormControl(''),
      cbLunes: new FormControl(false),
      cbMartes: new FormControl(false),
      cbMiercoles: new FormControl(false),
      cbJueves: new FormControl(false),
      cbViernes: new FormControl(false),
      cbSabado: new FormControl(false),
      cbDomingo: new FormControl(false)
    });

    this._requestService.getAllDetailPerson(this.second.id)
     .subscribe(response => {
        //Select first level
        this.firstLevelModel = response.result.firstLevelOrgans
        //Select head quarter
        this.headQuartersModel = response.result.headquarters
        //Table Detail Week
        this.authorizationDetail = response.result.authorization.authorizationDetail;
        //Select de Semanas
        let fechaStar = response.result.weeks;
        fechaStar.forEach((element, index) => {
          this.fechaModel.push({
            id: element.id,
            value: this.getSelectFecha(element.startDate, element.endDate),
            selected: index == 0 ? true : false
          })
        });

     });

     this._requestService.getAllPerson()
      .subscribe(response => {
        this.mockupPeople = response.result.items[0].person
      });
  }

  selectSecondLevel() {
    this._requestService.getAllDetailPerson(this.second.id)
    .subscribe(response => {
      this.secondLevelModel = response.result.secondLevelOrgans
      this.secondLevelModel = this.secondLevelModel.filter(data => data.parentId == this.formRequest.controls.cmbFirstId.value)
      // console.log("Prueba", this.mockupSecondLevel)
    });
  }

  selectThirdLevel(){
    // console.log("Id Tercero: ", this.formRequest.controls.cmbSecondId.value)
    this._requestService.getAllDetailPerson(this.second.id)
    .subscribe(response => {
      this.thirdLevelModel = response.result.thirdLevelOrgans
      this.thirdLevelModel = this.thirdLevelModel.filter(data => data.parentId == this.formRequest.controls.cmbSecondId.value)
      // console.log("Prueba", this.mockupThirdLevel)
    });
  }

  getSelectFecha(dtFechaStart: string, dtFechaEnd: string) {
    // console.log("Fechas: ", dtFechaStart + '-'+ dtFechaEnd)
    let dtStart = new Date(dtFechaStart);
    let dtEnd = new Date(dtFechaEnd);
    let diaS = dtStart.getDate();
    let diaE = dtEnd.getDate();
    // dtStart.setMonth(dtStart.getMonth)
    let mesS = dtEnd.getMonth() + 1;
    return `Semana: ${diaS} al ${diaE} de ${this.getNameMonth(mesS)}`;
  }

  getNameMonth(name: number | string) {
    let nombre = "";
    switch (name) {
      case 1:
        name = "Enero";
        break;
      case 2:
        name = "Febrero";
        break;
      case 3:
        name = "Marzo";
        break;
      case 4:
        name = "Abril";
        break;
      case 5:
        name = "Mayo";
        break;
      case 6:
        name = "Junio";
        break;

      case 7:
        name = "Julio";
        break;

      case 8:
        name = "Agosto";
        break;

      case 9:
        name = "Setiembre";
        break;

      case 10:
        name = "Octubre";
        break;

      case 11:
        name = "Noviembre";
        break;

      case 12:
        name = "Diciembre";
        break;
    }
    return name;
  }

  sendItemsRequest(){
    const {cmbWeekId, cmbFirstId, cmbSecondId, cmbThirdId, cmbHeadId, cbLunes, cbMartes, cbMiercoles, cbJueves, cbViernes, cbSabado, cbDomingo} = this.formRequest.value
    console.log("Id de semana: ", cmbWeekId)
    this.Item.push({
      id: 1,
      person: {
        id: 0
      },
      week: {
        id: cmbWeekId
      },
      state: 0,
      firstLevelOrgan: {
        id: cmbFirstId
      },
      secondLevelOrgan: {
        id: cmbSecondId
      },
      thirdLevelOrgan: {
        id: cmbThirdId
      },
      authorizationDetail:{
        order: 0,
        floor: 1,
        presentialDays: 1,
        monday: cbLunes,
        tuesday: cbMartes,
        wednesday: cbMiercoles,
        thursday: cbJueves,
        friday: cbViernes,
        saturday: cbSabado,
        sunday: cbDomingo,
        headquarterId: cmbHeadId
      }
    });
    // this.Item.person.id = 1
    // this.Item.week.id = cmbWeekId
    // this.Item.state = 0
    // this.Item.firstLevelOrgan.id = cmbFirstId
    // this.Item.secondLevelOrgan.id = cmbSecondId
    // this.Item.thirdLevelOrgan.id = cmbThirdId
    // // Item.headquarters = cmbHeadId,
    // this.Item.authorizationDetail.order = 1
    // this.Item.authorizationDetail.floor = 0
    // this.Item.authorizationDetail.presentialDays = 0
    // this.Item.authorizationDetail.monday = true
    // this.Item.authorizationDetail.tuesday = true
    // this.Item.authorizationDetail.wednesday = true
    // this.Item.authorizationDetail.thursday = true
    // this.Item.authorizationDetail.friday = true
    // this.Item.authorizationDetail.saturday = true
    // this.Item.authorizationDetail.sunday = true
    // this.Item.authorizationDetail.headquarterId = cmbHeadId
    const aea = this.Item
    console.log(JSON.stringify({aea}))
    this._requestService.sendItemsRequest(aea).subscribe(response => {
      console.log("Dentro de Subscribe: ", response)
    });
    console.log()
  }

  // getDataItem(){
  //   const {cmbWeekId, cmbFirstId, cmbSecondId, cmbThirdId, cmbHeadId} = this.formRequest.value
  //   this.Item.id = 0
  //   this.Item.person.id = 1
  //   this.Item.week.id = cmbWeekId
  //   this.Item.state = 0
  //   this.Item.firstLevelOrgan.id = cmbFirstId
  //   this.Item.secondLevelOrgan.id = cmbSecondId
  //   this.Item.thirdLevelOrgan.id = cmbThirdId
  //   // Item.headquarters = cmbHeadId,
  //   this.Item.authorizationDetail.order = 1
  //   this.Item.authorizationDetail.floor = 0
  //   this.Item.authorizationDetail.presentialDays = 0
  //   this.Item.authorizationDetail.monday = true
  //   this.Item.authorizationDetail.tuesday = true
  //   this.Item.authorizationDetail.wednesday = true
  //   this.Item.authorizationDetail.thursday = true
  //   this.Item.authorizationDetail.friday = true
  //   this.Item.authorizationDetail.saturday = true
  //   this.Item.authorizationDetail.sunday = true
  //   this.Item.authorizationDetail.headquarterId = cmbHeadId
  //   console.log("Item de: ", this.Item)
  //   return this.Item;
  // }

}
