import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { FirstOfficeComponent } from './pages/first-office/first-office.component';
import { SharedModule } from '../../shared/shared.module';
import { SecondOfficeComponent } from './pages/second-office/second-office.component';
import { ThirdOfficeComponent } from './pages/third-office/third-office.component';


@NgModule({
  declarations: [FirstOfficeComponent, SecondOfficeComponent, ThirdOfficeComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModule
  ]
})
export class DashboardModule { }
