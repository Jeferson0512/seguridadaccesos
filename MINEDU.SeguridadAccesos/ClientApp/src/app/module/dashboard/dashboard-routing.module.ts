import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FirstOfficeComponent } from './pages/first-office/first-office.component';
import { SecondOfficeComponent } from './pages/second-office/second-office.component';
import { ThirdOfficeComponent } from './pages/third-office/third-office.component';


const routes: Routes = [
  { path: 'first-office', component: FirstOfficeComponent },
  { path: 'second-office', component: SecondOfficeComponent },
  { path: 'third-office', component: ThirdOfficeComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
