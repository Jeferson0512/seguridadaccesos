import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThirdOfficeComponent } from './third-office.component';

describe('ThirdOfficeComponent', () => {
  let component: ThirdOfficeComponent;
  let fixture: ComponentFixture<ThirdOfficeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThirdOfficeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThirdOfficeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
