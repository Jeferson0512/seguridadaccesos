import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecondOfficeComponent } from './second-office.component';

describe('SecondOfficeComponent', () => {
  let component: SecondOfficeComponent;
  let fixture: ComponentFixture<SecondOfficeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecondOfficeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecondOfficeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
