import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstOfficeComponent } from './first-office.component';

describe('FirstOfficeComponent', () => {
  let component: FirstOfficeComponent;
  let fixture: ComponentFixture<FirstOfficeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirstOfficeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirstOfficeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
