import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginOfficeComponent } from './pages/login-office/login-office.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { RegisterPageComponent } from './pages/register-page/register-page.component';


const routes: Routes = [
  { path: 'login-page', component: LoginPageComponent },
  { path: 'register-page', component: RegisterPageComponent },
  { path: 'login-office', component: LoginOfficeComponent },
  { path: '**', redirectTo: 'auth/login-page'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
