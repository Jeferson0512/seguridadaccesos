import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly URL = environment.api;

  constructor(private _http: HttpClient) { }

  getAllPerson(): Observable<any>{
    return this._http.post(`${this.URL}/api/Autorizacion/autorizacion/ObtenerTodos`, {
      maxResultCount: 1000,
      skipCount: 0,
      sorting: "",
      filter: ""
    })
  }

  sendCredentials(email: string, password: string) {
    if (email == "jbujaico" && password == "123456") {
      return email;
    } else {
      return "";
    }
  }
}
