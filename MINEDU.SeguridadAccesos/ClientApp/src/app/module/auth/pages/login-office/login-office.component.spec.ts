import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginOfficeComponent } from './login-office.component';

describe('LoginOfficeComponent', () => {
  let component: LoginOfficeComponent;
  let fixture: ComponentFixture<LoginOfficeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginOfficeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginOfficeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
