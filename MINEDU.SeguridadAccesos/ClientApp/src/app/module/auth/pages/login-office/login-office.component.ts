import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-office',
  templateUrl: './login-office.component.html',
  styleUrls: ['./login-office.component.css']
})
export class LoginOfficeComponent implements OnInit {
  errorSession: boolean = false;
  formLogin: FormGroup = new FormGroup({});

  constructor(private _router: Router) { }

  ngOnInit() {
    this.formLogin = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(12)
      ])
    });
  }

  sendLoginOffice(): void {
    //const body = this.formLogin.value
    const { email, password } = this.formLogin.value
    //this._authService.sendCredentials(email, password)
    //  .subscribe(responseOK => {
    //    console.log(responseOK);
    //  });
    if (email == "jbujaico@gmail.com" && password == "123456") {
      this._router.navigate(['/dashboard/first-office']);
    } else if (email == "mleandro@gmail.com" && password == "123456") {
      this._router.navigate(['/dashboard/second-office']);
    } else if (email == "test@gmail.com" && password == "123456") {
      this._router.navigate(['/dashboard/third-office']);
    } else {
      this.errorSession = true;
      console.log("Error de usuario")
    }
  }

}
