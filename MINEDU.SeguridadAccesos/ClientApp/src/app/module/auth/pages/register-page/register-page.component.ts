import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.css']
})
export class RegisterPageComponent implements OnInit {

  formRegister: FormGroup = new FormGroup({});

  constructor() {
    this.formRegister = new FormGroup({
      codigo: new FormControl('')
    });
  }

  ngOnInit() {
  }
  sendRegister(): void {
    const { codigo } = this.formRegister.value
    if (codigo == "1") {
      document.getElementById("notiCuestionarioNoCompletado").style.display = "block";
      console.log("Entro al 1")
    } else {
      console.log("No entran")
    }
  }
}
