import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ItemsModel } from 'src/app/core/models/items.model';
import { PersonModel } from 'src/app/core/models/Person.model';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  errorSession: boolean = false;
  formLogin: FormGroup = new FormGroup({});
  itemsModel: Array<ItemsModel> = [];

  constructor(private _authService: AuthService, private _router: Router) { }

  ngOnInit() {
    this.formLogin = new FormGroup({
      cmbTipoPersona: new FormControl('',[
        Validators.required
      ]),
      email: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(12)
      ])
    });
  }

  // sendLogin(): void {
  //   //const body = this.formLogin.value
  //   const { email, password } = this.formLogin.value
  //   //this._authService.sendCredentials(email, password)
  //   //  .subscribe(responseOK => {
  //   //    console.log(responseOK);
  //   //  });
  //   if (email == "mleandro@gmail.com" && password == "123456") {
  //     this._router.navigate(['/solicitud']);
  //   } else {
  //     this.errorSession = true;
  //     console.log("Error de usuario")
  //   }
  // }

  sendLogin(){
    const { email, password, cmbTipoPersona } = this.formLogin.value
    this._authService.getAllPerson()
    .subscribe(response => {
      this.itemsModel = response.result.items
      console.log("Modelo Completo: ", this.itemsModel)
      this.itemsModel.forEach((model, index) => {
        if (model.person.email == email && model.person.identityDocument == password) {
          // this.itemsModel = this.itemsModel.filter(data => data.id == 1)
          // this.itemsModel = this.itemsModel.filter(data => data.person.id == 1)
          if(parseInt(cmbTipoPersona) == 1){
            this._router.navigate(['/solicitud']);
          }else if(parseInt(cmbTipoPersona) == 2){
            this._router.navigate(['/dashboard/first-office']);
          }
        }else{
              this.errorSession = true;
              console.log("Error de usuario")
        }
      });
    });
  }

}
