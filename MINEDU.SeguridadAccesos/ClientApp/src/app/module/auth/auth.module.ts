import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { SharedModule } from '../../shared/shared.module';
import { RegisterPageComponent } from './pages/register-page/register-page.component';
import { LoginOfficeComponent } from './pages/login-office/login-office.component';


@NgModule({
  declarations: [LoginPageComponent, RegisterPageComponent, LoginOfficeComponent],
  imports: [
    CommonModule,
    AuthRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class AuthModule { }
