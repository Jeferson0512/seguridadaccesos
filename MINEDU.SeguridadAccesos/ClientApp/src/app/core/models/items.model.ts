import { AuthorizationModel } from "./Authorization.model";
import { FirstLevelModel } from "./firstLevel.model";
import { FirstLevelOrgansModel } from "./firstLevelOrgans.model";
import { HeadQuarters } from "./headQuarters.model";
import { PersonModel } from "./Person.model";
import { SecondLevelModel } from "./secondLevel.model";
import { SecondLevelOrgansModel } from "./secondLevelOrgans.model";
import { ThirdLevelModel } from "./thirdLevel.model";
import { ThirdLevelOrgansModel } from "./thirdLevelOrgans.model";
import { WeekModel } from "./week.model";

export interface ItemsModel {
  id: number;
  person?: PersonModel;
  week?: WeekModel;
  applicationDate?: string;
  state?: number;
  firstLevelOrgan?: FirstLevelModel;
  secondLevelOrgan?: SecondLevelModel;
  thirdLevelOrgan?: ThirdLevelModel;
  headquarters?: HeadQuarters;
  creationTime?: string;
  firstLevelOrgans?: FirstLevelOrgansModel;
  secondLevelOrgans?: SecondLevelOrgansModel;
  thirdLevelOrgans?: ThirdLevelOrgansModel;
  authorizationDetail?: AuthorizationModel
}
