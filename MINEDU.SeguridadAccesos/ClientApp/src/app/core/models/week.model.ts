export interface WeekModel {
  id: number;
  startDate?: string;
  endDate?: string;
  state?: number;
}
