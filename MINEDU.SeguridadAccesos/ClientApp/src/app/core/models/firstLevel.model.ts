export interface FirstLevelModel {
  id: number;
  value?: string;
  abbreviation?: string;
  parentId?: number
}
