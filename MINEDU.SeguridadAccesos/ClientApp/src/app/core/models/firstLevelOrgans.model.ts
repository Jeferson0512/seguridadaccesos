export interface FirstLevelOrgansModel {
  id: number;
  value: string;
  abbreviation: string;
  parentId: number
}
