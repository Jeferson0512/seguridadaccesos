export interface PersonModel {
  id: number;
  typeDocument?: string;
  identityDocument?: string;
  surnames?: string;
  names?: string;
  email?: string;
}
