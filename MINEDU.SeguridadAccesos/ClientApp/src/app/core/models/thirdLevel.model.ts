export interface ThirdLevelModel {
  id: number;
  value?: string;
  abbreviation?: string;
  parentId?: number;
}
