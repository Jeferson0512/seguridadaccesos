export interface ThirdLevelOrgansModel {
  id: number;
  value: string;
  abbreviation: string;
  parentId: number;
}
