export interface HeadQuarters {
  id: number;
  abbreviation?: string;
  parentId?: number;
  value?: string;
}
