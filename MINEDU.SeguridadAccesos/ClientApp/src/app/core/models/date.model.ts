export interface DateModel{
  id: number;
  value: string;
  selected: boolean;
}
