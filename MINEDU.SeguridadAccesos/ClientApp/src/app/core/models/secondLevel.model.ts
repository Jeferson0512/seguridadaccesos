export interface SecondLevelModel {
  id: number;
  value?: string;
  abbreviation?: string;
  parentId?: number;
}
