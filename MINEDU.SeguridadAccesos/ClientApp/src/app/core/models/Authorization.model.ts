export interface AuthorizationModel{
  order: number;
  floor: number;
  presentialDays: number;
  monday: boolean;
  tuesday: boolean;
  wednesday: boolean;
  thursday: boolean;
  friday: boolean;
  saturday: boolean;
  sunday: boolean;
  headquarterId: number
}
