export interface SecondLevelOrgansModel {
  id: number;
  value: string;
  abbreviation: string;
  parentId: number;
}
